<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Location;
use App\Entity\Modele;
use App\Entity\User;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UnitTest extends TestCase
{
    // Test Unitaire pour le client //
    public function testClient(): void
    {
        $client = new Client();

        $client->setNom("Kahlaoui");
        $client->setPrenom("Amine");
        $client->setAdresse("23 impass el ward ,zahrouni ,Tunisia");
        $client->setCin("11667848");
        
        $this->assertSame("Amine", $client->getPrenom());
	$this->assertSame("Kahlaoui", $client->getNom());
	$this->assertSame("23 impass el ward ,zahrouni ,Tunisia", $client->getAdresse());
        $this->assertSame("11667848", $client->getCin());

    }
        // Test Unitaire pour la Location //
    public function testLocation(): void
    {
        $location = new Location();


        $dateDebut = new \DateTime('2024-01-01');
        $location->setDateD($dateDebut);
        $this->assertSame($dateDebut, $location->getDateD());

        $dateFin = new \DateTime('2024-01-07');
        $location->setDateA($dateFin);
        $this->assertSame($dateFin, $location->getDateA());


        $location->setPrix(2800.0);
        $this->assertSame(2800.0, $location->getPrix());


        $client = new Client();
        $location->setClient($client);
        $this->assertSame($client, $location->getClient());


        $voiture = new Voiture();
        $location->setVoiture($voiture);
        $this->assertSame($voiture, $location->getVoiture());


        $this->assertInstanceOf(Location::class, $location);
    }

    //Test Unitaire pourla Voiture

    public function testVoiture(): void
    {
        $voiture = new Voiture();


        $voiture->setSerie('200 Tunis 200');
        $this->assertSame('200 Tunis 200', $voiture->getSerie());

        $dateMM = new \DateTime('2016-07-13');
        $voiture->setDateMM($dateMM);
        $this->assertSame($dateMM, $voiture->getDateMM());

        $voiture->setPrixJour(400.0);
        $this->assertSame(400.0, $voiture->getPrixJour());

        $location = new Location();
        $voiture->addLocation($location);
        $this->assertCount(1, $voiture->getLocations());

        $voiture->removeLocation($location);
        $this->assertCount(0, $voiture->getLocations());


        $modele = new Modele();
        $voiture->setModele($modele);
        $this->assertSame($modele, $voiture->getModele());

        $this->assertInstanceOf(Voiture::class, $voiture,);
    }

    //Test Unitaire pour le  User
    public function testUser(): void
    {
        $user = new User();

        $user->setEmail('amine.kahlaoui01@gmail.com');
        $this->assertSame('amine.kahlaoui01@gmail.com', $user->getEmail());

        $this->assertSame('amine.kahlaoui01@gmail.com', $user->getUserIdentifier());
        
        $roles = ['ROLE_ADMIN', 'ROLE_USER'];
        $user->setRoles($roles);
        $this->assertContains('ROLE_USER', $user->getRoles());
        $this->assertContains('ROLE_ADMIN', $user->getRoles());

        $user->setPassword('hashed_password');
        $this->assertSame('hashed_password', $user->getPassword());

        $this->assertInstanceOf(UserInterface::class, $user);
        $this->assertInstanceOf(PasswordAuthenticatedUserInterface::class, $user);
    }

    //Test Unitaire pour le Modele

    public function testModele(): void
    {
        $modele = new Modele();


        $modele->setLibelle('FORD');
        $this->assertSame('FORD', $modele->getLibelle());


        $modele->setPays('Deaborn,Michigan');
        $this->assertSame('Deaborn,Michigan', $modele->getPays());


        $voiture = new Voiture();
        $modele->addVoiture($voiture);
        $this->assertCount(1, $modele->getVoitures());
        $this->assertTrue($modele->getVoitures()->contains($voiture));

        $modele->removeVoiture($voiture);
        $this->assertCount(0, $modele->getVoitures());
        $this->assertFalse($modele->getVoitures()->contains($voiture));


        $this->assertInstanceOf(Modele::class, $modele);
        
    }
}

