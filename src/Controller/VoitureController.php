<?php

namespace App\Controller;

use App\Entity\Voiture;
use App\Form\VoitureForm;
use App\Repository\VoitureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class VoitureController extends AbstractController
{
    #[IsGranted("ROLE_CLIENT")]
    #[Route('/voiture', name: 'app_voiture')]
    public function listeVoiture(VoitureRepository $voitureRepository): Response
    {
        $voitures = $voitureRepository->findAll();
        return $this->render('voiture/listeVoiture.html.twig', [
            'listeVoiture' => $voitures,
        ]);
    }

    #[Route('/addVoiture', name: 'addVoiture')]
    public function addVoiture(Request $request, EntityManagerInterface $entityManager): Response
    {
        $voiture = new Voiture();
        $form = $this->createForm(VoitureForm::class, $voiture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($voiture);
            $entityManager->flush();
            return $this->redirectToRoute('app_voiture');
        }

        return $this->render('voiture/addVoiture.html.twig', [
            'formV' => $form->createView(),
        ]);
    }

    #[Route('/voiture/{id}', name: 'voitureDelete')]
    public function delete(EntityManagerInterface $entityManager, VoitureRepository $voitureRepository, $id): Response
    {
        $voiture = $voitureRepository->find($id);
        if ($voiture) {
            $entityManager->remove($voiture);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_voiture');
    }

    #[Route('/updateVoiture/{id}', name: 'voitureUpdate')]
    public function updateVoiture(
        Request $request,
        EntityManagerInterface $entityManager,
        VoitureRepository $voitureRepository,
        $id
    ): Response {
        $voiture = $voitureRepository->find($id);
        if (!$voiture) {
            return $this->redirectToRoute('app_voiture');
        }

        $form = $this->createForm(VoitureForm::class, $voiture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('app_voiture');
        }

        return $this->render('voiture/updateVoiture.html.twig', [
            'editFormVoiture' => $form->createView(),
        ]);
    }

    #[Route('/searchVoitureModele', name: 'voitureSearchM')]
    public function searchVoitureModele(
        Request $request,
        EntityManagerInterface $em
    ): Response {
        $voitures = null;

        if ($request->isMethod('POST')) {
            $libelle = $request->request->get("input_libelle");
            $query = $em->createQuery(
                "SELECT v FROM App\Entity\Voiture v 
                JOIN v.modele m WHERE m.libelle LIKE :libelle"
            )->setParameter('libelle', '%' . $libelle . '%');

            $voitures = $query->getResult();
        }

        return $this->render('voiture/rechercheVoitureModele.html.twig', [
            'voitures' => $voitures,
        ]);
    }
}
